var assert = require("assert");
var submitButton = document.getElementById("audio_file_submit");
var fftSize = document.getElementById("fftSize");
var smoothness = document.getElementById("smoothness");
var magnification = document.getElementById("magnification");
var audioFilePath = document.getElementById("audio_file_path");
var canvas = document.getElementById("visualization");
//const canvas = new OffscreenCanvas(1920, 1080);
var audioControls = document.getElementById("audioControls");
var visualizer = document.getElementById("visualizer");
var burst_ratio = document.getElementById("burst_ratio");
var record = document.getElementById("record");
var WIDTH = 1920;
var HEIGHT = 1080;
var RUNNING_AVG_SMOOTHNESS = parseFloat(smoothness.value);
var MAGNIFICATION = parseFloat(magnification.value);
var BURST_RATIO = parseFloat(burst_ratio.value);
var minForNormaliztion = 127.0;
var maxForNormaliztion = 129.0;
var space = new Image(WIDTH, HEIGHT);
space.src = "space.jpeg";
var earth = new Image(766, 765);
earth.src = "earth.png";
var visualizers = {
    "Oscilloscope": drawOscilloscope,
    "Bar Oscilloscope": drawBarOscilloscope,
    "Circle Oscilloscope": drawCircleOscilloscope
};
for (var key in visualizers) {
    var option = document.createElement('option');
    option.value = key;
    option.innerHTML = key;
    visualizer.appendChild(option);
}
var canvasCtx = canvas.getContext("2d");
canvas.height = HEIGHT;
canvas.width = WIDTH;
var audioCtx = new (window.AudioContext)();
submitButton.onclick = analyzeLive;
var analyser = audioCtx.createAnalyser();
fftSize.onchange = function () { analyser.fftSize = parseInt(fftSize.value); };
smoothness.onchange = function () { RUNNING_AVG_SMOOTHNESS = parseFloat(smoothness.value); };
magnification.onchange = function () { MAGNIFICATION = parseFloat(magnification.value); };
burst_ratio.onchange = function () { BURST_RATIO = parseFloat(burst_ratio.value); };
var createCanvasRecorder = require("canvas-record");
var canvasRecorder = createCanvasRecorder(canvas, {
    frameRate: 30, audio: audioControls
});
var history_circle = new Array();
var history_amplitude = new Array();
record.onclick = function () {
    if (record.textContent == "Stop Recording") {
        record.textContent = "Start Recording";
        canvasRecorder.stop();
        canvasRecorder.dispose();
    }
    else {
        canvasRecorder.start();
        audioControls.play();
        record.textContent = "Stop Recording";
    }
};
function analyzeLive() {
    if (audioFilePath.files.length > 0) {
        var file = audioFilePath.files[0];
        audioControls.src = file.path;
    }
    var source = audioCtx.createMediaElementSource(audioControls);
    source.connect(analyser);
    analyser.connect(audioCtx.destination);
    var runningAvg = new Float32Array(analyser.frequencyBinCount);
    drawOnline(analyser, runningAvg);
}
;
function drawOnline(analyser, runningAvg) {
    var dataArray = new Uint8Array(analyser.frequencyBinCount);
    analyser.getByteTimeDomainData(dataArray);
    draw(dataArray, runningAvg);
    window.requestAnimationFrame(function () { drawOnline(analyser, runningAvg); });
}
var RecordRTC = require('recordrtc');
var Whammy = RecordRTC.Whammy;
function analyzeOffline() {
    var file = audioFilePath.files[0];
    file.arrayBuffer().then(function (buffer) {
        audioCtx.decodeAudioData(buffer, function (audio_buffer) {
            var runningAvg = new Float32Array(2048);
            var path = file.path;
            path += "-frames/";
            if (!fs.existsSync(path)) {
                fs.mkdirSync(path);
            }
            window.requestAnimationFrame(function () { drawOffline(path, 0, audio_buffer, runningAvg); });
        });
    });
}
;
function dataURItoBlob(dataURI) {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    var byteString = atob(dataURI.split(',')[1]);
    // separate out the mime component
    var mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    // write the bytes of the string to an ArrayBuffer
    var ab = new ArrayBuffer(byteString.length);
    // create a view into the buffer
    var ia = new Uint8Array(ab);
    // set the bytes of the buffer to the correct values
    for (var i = 0; i < byteString.length; i++) {
        ia[i] = byteString.charCodeAt(i);
    }
    // write the ArrayBuffer to a blob, and you're done
    var blob = new Blob([ab], { type: mimeString });
    return blob;
}
var fs = require('fs');
function drawOffline(path, frame, audio_buffer, runningAvg) {
    console.log(frame);
    if ((audio_buffer.sampleRate * frame / 60.) > audio_buffer.length) {
        return;
    }
    var channel = new Float32Array(2048);
    audio_buffer.copyFromChannel(channel, 0, (frame / 60.) * audio_buffer.sampleRate);
    var dataArray = new Uint8Array(2048);
    for (var i = 0; i < channel.length; i++)
        dataArray[i] = (channel[i] * 128) + 128;
    draw(dataArray, runningAvg);
    canvas.toBlob(function (blob) {
        blob.arrayBuffer()
            .then(function (buffer) { return fs.writeFileSync(path + 'img-' + frame + '.png', new Buffer(buffer)); });
    }, "image/png", 1);
    window.requestAnimationFrame(function () { drawOffline(path, frame + 1, audio_buffer, runningAvg); });
}
function draw(dataArray, runningAvg) {
    canvasCtx.fillStyle = "black";
    canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);
    //canvasCtx.drawImage(space, 0, 0);
    var fn = visualizers[visualizer.value];
    fn(dataArray, runningAvg);
}
function drawOscilloscope(dataArray, runningAvg) {
    canvasCtx.strokeStyle = 'rgb(100, 100, 100)';
    canvasCtx.lineWidth = 2;
    var bufferLength = dataArray.length;
    var sliceWidth = WIDTH * 1.0 / bufferLength;
    var x = 0;
    canvasCtx.beginPath();
    var lravg = 128.;
    console.log(Math.max.apply(Math, dataArray));
    for (var i = 0; i < bufferLength; i++) {
        var w = (dataArray[i] - 128.) * MAGNIFICATION + 128.;
        runningAvg[i] = runningAvg[i] * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * w;
        lravg = lravg * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * runningAvg[i];
        var v = lravg / 128.0;
        var y = v * HEIGHT / 2;
        if (i === 0)
            canvasCtx.moveTo(x, y);
        else
            canvasCtx.lineTo(x, y);
        x += sliceWidth;
    }
    canvasCtx.stroke();
}
function drawBarOscilloscope(dataArray, runningAvg) {
    var bufferLength = dataArray.length;
    var sliceWidth = WIDTH * 1.0 / bufferLength;
    var x = 0;
    canvasCtx.beginPath();
    var lravg = 128.;
    for (var i = 0; i < bufferLength; i++) {
        var w = (dataArray[i] - 128.) * MAGNIFICATION + 128.;
        runningAvg[i] = runningAvg[i] * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * w;
        lravg = lravg * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * runningAvg[i];
        var v = lravg / 128.0;
        var y = v * HEIGHT / 2;
        canvasCtx.fillStyle = 'rgb(100,100,100)';
        canvasCtx.fillRect(x, HEIGHT / 2, sliceWidth, y - HEIGHT / 2);
        canvasCtx.lineWidth = 2;
        canvasCtx.strokeStyle = 'rgb(0, 0, 0)';
        if (i === 0)
            canvasCtx.moveTo(x, y);
        else
            canvasCtx.lineTo(x, y);
        x += sliceWidth;
    }
    canvasCtx.stroke();
}
// pos should be between 0 and 1
function getColor(pos) {
    return 'hsla(' + pos * 500 + ',50%, 40%,50%)';
}
function getRandomInt(max) {
    return Math.floor(Math.random() * Math.floor(max));
}
var Star = /** @class */ (function () {
    function Star() {
        this.radius = 1;
    }
    return Star;
}());
;
var stars = [];
for (var i = 0; i < 200; i++) {
    var star = new Star();
    star.cx = getRandomInt(WIDTH);
    star.cy = getRandomInt(HEIGHT);
    star.radius = getRandomInt(3);
    star.vy = Math.random() * 3;
    star.vx = Math.random() * 0.1;
    stars.push(star);
}
stars[0].radius = 0;
function drawCircleOscilloscope(dataArray, runningAvg) {
    var bufferLength = dataArray.length;
    var mx = Math.max.apply(Math, runningAvg);
    var pmx = mx;
    if (history_amplitude != null && history_amplitude.length > 0)
        pmx = history_amplitude[history_amplitude.length - 1];
    var burst = true;
    if (mx < pmx * BURST_RATIO) {
        burst = false;
        mx = RUNNING_AVG_SMOOTHNESS * pmx + (1 - RUNNING_AVG_SMOOTHNESS) * mx;
    }
    var r = Math.min(HEIGHT, WIDTH) / 2.5;
    r *= mx / 256.0 * 0.8;
    var cx = WIDTH / 2;
    var cy = HEIGHT / 2;
    canvasCtx.drawImage(earth, cx - r, cy - r, 2 * r, 2 * r);
    var sliceWidth = 2 * Math.PI * r / bufferLength;
    var color = getColor(mx / 256.0);
    canvasCtx.beginPath();
    canvasCtx.lineWidth = 2;
    canvasCtx.strokeStyle = color;
    var lravg = 128.;
    var cur_circle = new Array(bufferLength);
    for (var i = 0; i < bufferLength; i++) {
        var w = (dataArray[i] - 128.) * MAGNIFICATION + 128.;
        runningAvg[i] = runningAvg[i] * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * w;
        lravg = lravg * RUNNING_AVG_SMOOTHNESS + (1 - RUNNING_AVG_SMOOTHNESS) * runningAvg[i];
        var v = lravg / 128.0;
        var y = r * (v - 1);
        canvasCtx.fillStyle = color;
        var rads = 2 * Math.PI * i / bufferLength;
        var base_x = r * Math.cos(rads);
        var base_y = r * Math.sin(rads);
        canvasCtx.fillRect(cx + base_x, cy + base_y, sliceWidth, y);
        cur_circle[i] = [cx + base_x, cy + base_y + y];
    }
    canvasCtx.stroke();
    var total_circles = 100.;
    if (history_circle.length > total_circles) {
        history_circle.shift();
        history_amplitude.shift();
    }
    for (var i = 0; i < history_circle.length; i++) {
        if (history_circle[i].length == 0)
            continue;
        var distance = (1 - (i / total_circles));
        canvasCtx.beginPath();
        canvasCtx.lineWidth = 4;
        canvasCtx.strokeStyle = 'hsla(' + (history_amplitude[i] / 256.0) * 500 + ',' + (1 - distance) * 50 + '%' + ',' + (1 - distance) * 40 + '%,50%)';
        var _a = [0, 0], fx = _a[0], fy = _a[1];
        for (var j = 0; j < history_circle[i].length; j++) {
            var _b = history_circle[i][j], hx = _b[0], hy = _b[1];
            var amp = Math.sqrt(Math.pow(hx - cx, 2) + Math.pow(hy - cy, 2));
            hx += ((hx - cx) / amp) * distance * WIDTH / 3;
            hy += ((hy - cy) / amp) * distance * WIDTH / 3;
            if (j == 0) {
                fx = hx;
                fy = hy;
                canvasCtx.moveTo(hx, hy);
            }
            else {
                canvasCtx.lineTo(hx, hy);
            }
        }
        canvasCtx.lineTo(fx, fy);
        canvasCtx.stroke();
    }
    for (var i = 0; i < stars.length; i++) {
        stars[i].cy += 0.1 * Math.pow(Math.E, 4 * mx / 256.0) * stars[i].vy;
        stars[i].cx += stars[i].vx;
        if (stars[i].cy > HEIGHT)
            stars[i].cy = 0;
        if (stars[i].cx > WIDTH)
            stars[i].cx = 0;
        var x = getRandomInt(255);
        canvasCtx.fillStyle = 'rgba(' + x + ',' + x + ',' + x + ',' + getRandomInt(50) + 50 + ')';
        canvasCtx.beginPath();
        canvasCtx.arc(stars[i].cx, stars[i].cy, stars[i].radius, 0, 2 * Math.PI, true);
        canvasCtx.closePath();
        canvasCtx.fill();
    }
    if (burst == false) {
        cur_circle = [];
    }
    if (mx != 128) {
        history_circle.push(cur_circle);
        history_amplitude.push(mx);
    }
    canvasCtx.stroke();
}
//# sourceMappingURL=render.js.map